Solarus community had been very active for the last months. People started developing games and some are pretty far advanced. So we had to create a <a href="http://www.solarus-games.org/games/community/">new page on Solarus website to feature community's games</a>.

Two games are well advanced in their development:
<ul>
	<li><em><a href="http://tunics.legofarmen.se/">Tunics!</a></em>: a rogue-like Zelda game. It means it's only dungeons, and they are randomly generated. If you die, you'll have to start again from the beginning.</li>
	<li><em><a href="http://forum.solarus-games.org/index.php/topic,71.0.html">The Legend of Zelda<strong>�</strong>: Book of Mudora</a></em>: a more classic Zelda game.</li>
</ul>
Have fun !