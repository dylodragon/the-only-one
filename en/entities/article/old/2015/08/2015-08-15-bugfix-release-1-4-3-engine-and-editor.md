Our new game <a href="http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/">Zelda Return of the Hylian</a> (Solarus Edition) comes with a new bugfix release of Solarus and Solarus Quest Editor. A lot of issues that you reported on the bug tracker were solved, as well as some problems I detected while working on Return of the Hylian.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/engine/download/">Download Solarus 1.4</a></li>
	<li><a href="http://www.solarus-games.org/engine/solarus-quest-editor/">Solarus Quest Editor</a></li>
	<li><a title="LUA API documentation" href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
	<li><a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">Migration guide</a></li>
</ul>
<h2>Changes in Solarus 1.4.3</h2>
<ul>
	<li>Fix crash at exit when a surface has a movement with callback (#699).</li>
	<li>Fix crash when removing a custom entity (#690).</li>
	<li>Fix crash when a sprite file is missing or has no animation (#700).</li>
	<li>Fix crash when trying to remove a sprite already removed (#705).</li>
	<li>Fix crash when a custom entity collision or traversable test errors.</li>
	<li>Fix crash when changing hero sprites sometimes.</li>
	<li>Fix crash when sound buffers are full.</li>
	<li>Fix crash in map:get_ground() with out of bounds coordinates.</li>
	<li>Fix Lua error message saying "number expected" instead of "string expected".</li>
	<li>Fix game:set_command_keyboard/joypad_binding refusing parameters.</li>
	<li>Fix map scrolling not working if quest size is not a multiple of 5 (#701).</li>
	<li>Fix camera:move() ignoring separators.</li>
	<li>Fix entities already destroyed when map:on_finished() is called (#691).</li>
	<li>Fix entity:bring_to_front()/back() ignoring the order of obstacles.</li>
	<li>Fix hero stuck on blocks.</li>
	<li>Fix hero going backwards on ice sometimes.</li>
	<li>Fix custom_entity:set_can_traverse_ground() giving opposite result (#668).</li>
	<li>Fix enemy:immobilize() having no effect when already immobilized.</li>
	<li>Fix dying animation of flying and swimming enemies.</li>
	<li>Fix the position of the shadow of pickables when they move.</li>
	<li>Fix pickables not reacting to their ground (#655).</li>
	<li>Fix a compilation error with Mac OS X.</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.4.3</h2>
<ul>
	<li>Quest properties editor: fix setting the write directory field empty (#36).</li>
	<li>Sprite editor: add a button to refresh the source image (#50).</li>
	<li>Map editor: fix crash when resizing with no entities selected (#51).</li>
	<li>Map editor: fix entities still visible when adding them on a hidden layer.</li>
	<li>Map editor: fix entity dialog allowing to set illegal sizes (#23).</li>
	<li>Map editor: fix changing the direction of a jumper from the dialog (#60).</li>
	<li>Map editor: fix sprites not always updated when changing direction (#32).</li>
	<li>Map editor: show a context menu when right-clicking an empty space (#26).</li>
	<li>Tileset editor: fix usability issues to create and select patterns (#31).</li>
	<li>Tileset editor: fix moving a pattern to a partially overlapping place (#29).</li>
	<li>Tileset editor: fix color when moving a pattern to an occupied place (#34).</li>
	<li>Tileset editor: fix existing selection lost if selecting with ctrl or shift.</li>
	<li>Text editor: the find button is now the default one in the find dialog (#30).</li>
	<li>Dialogs editor: fix crash when comparing empty dialogs (#48).</li>
	<li>Dialogs editor: ensure that the text ends with a newline (#45).</li>
	<li>Make the numpad enter key work like the return key (#49).</li>
	<li>Check the Solarus library version at compilation time (#41).</li>
</ul>
Enjoy!