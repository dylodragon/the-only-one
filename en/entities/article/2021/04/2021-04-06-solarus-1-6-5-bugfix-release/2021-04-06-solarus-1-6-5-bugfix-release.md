We have just published a bugfix release for Solarus, which is now at version **1.6.5**.

## Changelog

Here is the exhaustive list of all changes brought by Solarus 1.6.5. Thanks to all contributors.

### Changes for Solarus 1.6.5

#### Engine changes

* Add support for suspending the simulation on Window Focus events (!1352).
* Fix crash when calling command functions before the game is started ([#1476](https://gitlab.com/solarus-games/solarus/-/issues/1476)).
* Fix joystick hot-plug/unplug ([#1501](https://gitlab.com/solarus-games/solarus/-/issues/1501), [#1387](https://gitlab.com/solarus-games/solarus/-/issues/1387)).
* Fix TTF fonts using wrong color for antialiasing ([#1390](https://gitlab.com/solarus-games/solarus/-/issues/1390)).
* Fix TTF Fonts not rendering correctly ([#1389](https://gitlab.com/solarus-games/solarus/-/issues/1389)).
* Fix a possible crash when starting a timer on a removed entity ([#1469](https://gitlab.com/solarus-games/solarus/-/issues/1469)).
* Fix pickables not falling on negative layers.

#### Lua API changes

This release adds new features but does not introduce any incompatibility.

* Add methods `destructible:get/set_cut_method()` ([#1526](https://gitlab.com/solarus-games/solarus/-/issues/1526)).

#### Solarus launcher GUI changes

* Add option to control suspension on Window Focus events.
* Rename *Force Software* to *Force software rendering*.
* Disable force-software and suspend-unfocused options when quest is running.

### Changes for Solarus Quest Editor 1.6.5

* Map editor: fix crash when generating contours with missing patterns ([#494](https://gitlab.com/solarus-games/solarus-quest-editor/-/issues/494)).
* Map editor: fix resize and edit shortcuts not working sometimes ([#272](https://gitlab.com/solarus-games/solarus-quest-editor/-/issues/272)).
