# Collectibles

## Magic Bottle

### 01 - Bakery

Bring six apples to the baker (25 rupees for 3 apples in the village general store) and he will give you the first Magic Bottle.

[Video link : Bakery (Magic Bottle)](http://www.youtube.com/watch?v=FanqKKhfilk#t=660s)

### 02 - Hyrule Castle Waterfall

Once you have the flippers, swim along the Castle and enter the waterfall to the north of it. The second Magic Bottle is in a chest.

[Video link : Hyrule's Castle Waterfall (Magic Bottle)](http://www.youtube.com/watch?v=x84d8Lh24Mg#t=333s)

### 03 - Witch's Hut

Once you can access Inferno, go in the Witch's hut on the right of it. Buy a potion (anyone) and the witch will offer you the third Magic Bottle.

[Video link : Witch's Hut (Magic Bottle)](http://www.youtube.com/watch?v=jbUnejEjXrs#t=730s)

### 04 -Mount Terror

After using the Mystic Mirror on the waterfall of the Mount Terror, fall from the first cliff you see. Follow the path by falling in holes, and after 2 floors you'll stand before the chest containing the fourth and last Magic Bottle.

[Video link : Mount Terror (Magic Bottle)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=940s)

## Tiger Scroll

To get the ultimate charged attack, go the the west Mount Terror and go thru the first bridge, then go east. Light up all torches after get rid of the black stones, a Fairy appears and teaches you the attack.

[Video link : Mount Terror (Tiger Scroll )](http://www.youtube.com/watch?v=0mQrcTSgTt8#t=80s)
