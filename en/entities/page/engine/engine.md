[container]

[align type="center"]

# {title}

[hr width="5"]

[/align]

[row]
[column width="7"]

![Illustration](images/illustration.svg)

[/column]

[column width="1"]
[space orientation="vertical"]
[/column]

[column width="4"]

[space thickness="100"]

[button-highlight type="primary" icon="presentation" url="/en/solarus/overview" label="Overview"]

[button-highlight type="primary" icon="download" url="/en/solarus/download" label="Download"]

[button-highlight type="primary" icon="calendar" url="/en/solarus/changelog" label="Changelog"]

[/column]
[/row]

[/container]
