### Presentation

Every game studio has its vaporware, and *The Legend of Zelda: Mercuris Chest* is ours! The game has been in development since 2003. A demo, made with Multimedia Fusion, was released in 2003, with a huge, long and intricate dungeon. However, development was cancelled in 2007... only to be remade from scratch in 2013, with Solarus this time!

This is Solarus Team's most ambitious game, and it intends to be the definitive 2D Zelda-like game. The overworld is a huge open-world kingdom, with lots of different landscapes, lots of NPCs and quests. The dungeons should be as huge as the one in the demo, and better designed than in *Mystery of Solarus DX*. Custom sprites (PNJ, enemies, bosses), tilesets and elements have been created to give a special charm the to the game. 
Moreover, the story is original, and more developped than the one in *Mystery of Solarus*.

Interestingly, a lot of the ideas we had in 2003 have been used in official *Zelda* games since. This reinforces that we had good ideas that were in line with the *Zelda* series.

![Link](artworks/artwork_link.png "Link")

### Synopsis

Link was quietly taking a nap in his house, when suddenly 2 strangers kidnapped him and brought him to an unknown kingdom. When he finally woke up, a priest explained that they need Link's help to fight against a giant bird who tries to steal a legendary chest, containing a dangerous weapon. What's in this chest? And who is that mysterious bird warrior? And why this kingdom seems so familiar, but also so different? You will discover everything by playing *Mercuris' Chest*!

![Mercuris](artworks/artwork_mercuris.png "Mercuris")
