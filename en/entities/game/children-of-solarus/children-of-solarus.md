### Presentation

*Children of Solarus* is the 100% free remake of *The Legend of Zelda: Mystery of Solarus DX*. All proprietary content is replaced by 100% libre content: custom sprites, tilesets, musics, sounds, and so on! Story and characters are a bit expanded to make the game stand on its own, instead of using *Zelda* lore.
