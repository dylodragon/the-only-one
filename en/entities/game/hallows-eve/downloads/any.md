### Download Solarus

To play a Solarus quest, you need the **Solarus Launcher** application. Please make sure you have [installed it first](/en/solarus/download).

If you already have Solarus Launcher, you can skip to **step 2**.

### Download the quest

This game isn't directly available as a **.solarus file**. Click on the following button to go to the game's page on **itch.io**.
