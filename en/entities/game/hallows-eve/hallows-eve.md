### It's Halloween. It's the 90s. You're a Pumpkin Man.

On Halloween, the barrier between human world and the spirit world thins. The spirits play tricks, the dead can be seen, and monsters can run their human world errands.

Set in the 90s, play as Ichabod, a pumpkin man who uses his chance to enter the human world to try and rent Jurassic Park from the video store. Unfortunately, the only copy has been rented by a teen who's brought a witch's curse upon the town.

Gameplay is inspired by the classics and the modern classics — a Zeldalike sense of exploration, with the tight, fast combat of Hyper Light Drifter. All soaked in a charming pixel art atmosphere and a dumb sense of humor. Playtime is around 1-2 hours.

### Default Controls

| Key      | Action |
|:--------:|--------|
| <kbd>←</kbd> <kbd>↑</kbd> <kbd>→</kbd> <kbd>↓</kbd> | Move |
| <kbd>Space</kbd>                                    | Action / Dodge |
| <kbd>C</kbd>                                        | Kick |
| <kbd>X</kbd>                                        | Special Attack |
| <kbd>V</kbd>                                        | Heal |
| <kbd>D</kbd>                                        | Pause / Map |
| <kbd>D</kbd> (In Pause Menu)                        | Save and return to title screen |
| <kbd>F1</kbd>                                       | Control Mapping |
| <kbd>F2</kbd>                                       | Options |
