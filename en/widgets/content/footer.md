[container]
[row]
[column]

#### Legal Information

**© 2006-2020 Christopho, Solarus**</br>
Software is GPL. Resources are CC-BY-SA.

* [link title="Legal Terms" label="Legal Terms" url="/en/about/legal" icon="gavel" target=""]

[align type="center"]
[![GPL v3 logo](images/gpl_v3_logo.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)
[space orientation="vertical" thickness="10"]
[![CC-BY-SA 4.0 logo](images/cc_logo.png)](https://creativecommons.org/licenses/by-sa/4.0)
[/align]

[/column]
[column]

#### About

Solarus is made by a team of people on their free time.

* [link title="Frequently Asked Questions" label="Frequently Asked Questions" url="/en/about/faq" icon="question-circle" target=""]
* [link title="Contributors" label="Contributors" url="/en/about/contributors" icon="users" target=""]
* [link title="Nonprofit Organization" label="Nonprofit Organization" url="/en/about/nonprofit-organization" icon="balance-scale" target=""]
* [link title="Contact" label="Contact" url="/en/about/contact" icon="envelope-open-text" target=""]

[/column]
[column]

#### Contribute

Solarus is free: your help or support is always appreciated.

* [link title="How to contribute" label="How to contribute" url="/en/development/how-to-contribute" icon="hand-holding-heart" target=""]
* [link title="Donate" label="Donate" url="/en/development/donation" icon="donate" target=""]
* [link title="Source code" label="Source code" url="https://gitlab.com/solarus-games" icon="code-branch" target=""]
* [link title="Goodies" label="Goodies" url="https://shop.spreadshirt.fr/solarus-labs" icon="shopping-bag" target=""]

[/column]
[column]

#### Links

Useful links for people who want to keep being informed about the project.

[link title="Gitlab" url="https://gitlab.com/solarus-games" icon="gitlab" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Twitter" url="https://www.twitter.com/SolarusGames" icon="twitter" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Facebook" url="https://www.facebook.com/solarusgames" icon="facebook" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Mastodon" url="https://mastodon.gamedev.place/@solarus" icon="mastodon" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Discord" url="https://discord.gg/yYHjJHt" icon="discord" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Reddit" url="https://www.reddit.com/r/solarus" icon="reddit" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="RSS" url="https://www.solarus-games.org/feed/en/articles" icon="rss" icon-category="fa" icon-size="2x" target=""]

[/column]
[column]

#### Sponsors

We gracefully thank our sponsors for their help.

[![GPL v3 logo](images/macstadium_poweredby_logo.png)](https://www.macstadium.com/)
[/column]
[/row]

[row]
[column width="3"]
[space]
[/column]
[column width="6"]
[align type="center"]
[space orientation="vertical" thickness="10"]

Made with [Kokori](https://gitlab.com/solarus-games/kokori), our own free and open-source website engine.

[/align]
[/column]
[column width="3"]
[space]
[/column]

[/row]

[/container]
