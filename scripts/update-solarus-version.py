#!/usr/bin/env python3
# coding=utf-8

# Script to update Solarus version. It needs to be improved, but at least we
# don't waste time doing all this manually.
# What needs to be done (for every language):
# 1. Homepage /en/entities/page/home/home.md: version and release date
# 2. Downloads metadata /en/downloads/config.json: version
# 3. News article: add an new article in /en/entites/article/<yyyy>/<mm>
# 4. Generate the image for the news article
# Example: python scripts/update-solarus-version.py -version "1.6.4" -author "Christopho"

import argparse
from datetime import date
import re
import os
import fileinput
import locale
import sys
try:
  import urllib.request as urllib2
except ImportError:
  import urllib2
from PIL import Image, ImageDraw, ImageFont
import json

import jsonutils

# Updates the homepage with version number and release date in parameter.
# If no release_date, use today's date.
def update_homepage(version, release_date, verbose = False):
  translations = {
    "en": {
      "regexp": r"\[small\]Stable version (\d+\.\d+\.\d+) • Updated on (.+)\[\/small\]",
      "replacement": "[small]Stable version %(version)s • Updated on %(date)s[/small]",
      "date_template": r"%B %d, %Y",
      "locale": "en_GB.utf8",
    },
    "fr": {
      "regexp":  r"\[small\]Version stable (\d+\.\d+\.\d+) • Mise à jour le (.+)\[\/small\]",
      "replacement": "[small]Version stable %(version)s • Mise à jour le %(date)s[/small]",
      "date_template": r"%d %B %Y",
      "locale": "fr_FR.utf8",
    }
  }

  for lang in ["en", "fr"]:
    lang_data = translations[lang]
    
    # File to edit
    file_path = "./%(lang)s/entities/page/home/home.md" % { 
      "lang": lang
    }
    abs_file_path = os.path.abspath(file_path)

    # Read whole content and close the file 
    with open(abs_file_path, 'r', encoding="utf8") as f:
      content = f.read()
    
    # Replace line
    locale.setlocale(locale.LC_ALL, lang_data["locale"])
    formatted_release_date = release_date.strftime(lang_data["date_template"])
    replacement = lang_data["replacement"] % { "version":  version, "date": formatted_release_date} 
    result = re.sub(lang_data["regexp"], replacement, content, re.MULTILINE)
    
    # Write to file
    with open(abs_file_path, 'w', encoding="utf8") as f:
      f.write(result)

      if verbose:
        print("Updated:", file_path)

# Download changelog content.
def download_changelog(url, verbose = False):
  # Spoof user-agent.
  headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"
  }
  request = urllib2.Request(url, headers=headers)
  
  # Download (sync).
  if verbose:
    print("Downloading file", url)

  with urllib2.urlopen(request) as f:
    content = f.read().decode('utf-8')
  
  if verbose:
    print("Downloaded")

  return content

# Get the text content for the version as parameter.
def get_version_changelog(version, full_changelog, verbose = False):
  start_line_number = None
  line_number = -1
  pattern = re.compile(r"^## .+(\d+\.\d+\.\d+).+$")
  version_changelog_lines = []

  lines = full_changelog.splitlines()
  for line in lines:
    line_number += 1
    match = pattern.match(line)
    
    if match:
      # Find start of the requested changelog.
      if match.group(1) == version:
        start_line_number = line_number
      # Find start of following changelog: end of requested changelog.
      elif not start_line_number is None:
        break
    
    if not start_line_number is None:
      version_changelog_lines.append(line.strip())

  # Rremove 1st line (title)
  return "\n".join(version_changelog_lines[1:]).strip()

# JSON file.
def create_json_file(article_folder, json_file_template, article_id, version, release_date, author, verbose = False):
  # Containing folder.
  if not os.path.exists(article_folder):
    os.makedirs(article_folder)

  # Read template.
  with open(json_file_template, 'r', encoding="utf8") as f:
    json_file_template_content = f.read()

  json_rel_file = article_folder + "/" + article_id + ".json"
  json_file = os.path.abspath(json_rel_file)
  with open(json_file, 'w', encoding="utf8") as f:
    json_file_content = json_file_template_content % {
      "author": author,
      "release_date": release_date.isoformat(),
      "version": version,
      "version_url": version.replace(".", "-"),
    }
    f.write(json_file_content)
    
    if verbose:
      print("Created:", json_rel_file)  

# MD file.
def create_md_file(article_folder, md_file_template, article_id, version, content, verbose = False):
  # Containing folder.
  if not os.path.exists(article_folder):
    os.makedirs(article_folder)

  # Read template.
  with open(md_file_template, 'r', encoding="utf8") as f:
    md_file_template_content = f.read()

  md_rel_file = article_folder+ "/" + article_id + ".md"
  md_file = os.path.abspath(md_rel_file)
  with open(md_file, 'w', encoding="utf8") as f:
    md_file_content = md_file_template_content % {
      "version": version,
      "content": content,
    }
    
    f.write(md_file_content)

    if verbose:
      print("Created:", md_rel_file)

# Cover picture.
def create_image_file(article_folder, version, verbose = False):
  # Images folder
  images_folder = os.path.abspath(article_folder+ "/" + "images")
  if not os.path.exists(images_folder):
    os.makedirs(images_folder)
  
  # Thumbnail image
  cover_rel_filepath = article_folder+ "/" + "images/cover.jpeg"
  cover_filepath = os.path.abspath(cover_rel_filepath)
  
  if os.path.exists(cover_filepath):
    os.remove(cover_filepath)
  
  thumbnail_image = Image.open(os.path.abspath(r"./scripts/templates/article-release-cover.template.png"), mode="r")
  image_size = thumbnail_image.size
  draw = ImageDraw.Draw(thumbnail_image)
  font = ImageFont.truetype("UbuntuTitling-Bold", 100) # Please install the font
  text_size = font.getsize(version)
  text_pos_x = (image_size[0] - text_size[0]) / 2 # pixels
  text_pos_y = (image_size[1] - text_size[1]) / 2 + 60 # pixels
  draw.text((text_pos_x, text_pos_y), version, (255,255,255), font=font)
  thumbnail_image.save(cover_filepath, 'jpeg', quality=90, optimize=True, progressive=True)
  thumbnail_image.close()

  if verbose:
    print("Created:", cover_rel_filepath)

# Update the download URLs for the new version.
def update_downloads(version, verbose = False):
  for lang in ["en", "fr"]:
    downloads_path = "./%(lang)s/downloads/config.json" % {
      "lang": lang,
    }

    # Read the JSON file.
    with open(os.path.abspath(downloads_path), "r") as f:
      data = json.load(f)
    
    # Update player download.
    player_pkg_win = "https://gitlab.com/solarus-games/solarus/-/jobs/artifacts/v%(version)s/raw/solarus-player-x64-v%(version)s.zip?job=mingw-x64-package" % {
      "version": version,
    }
    player_downloads = data["downloads"]["player"]
    for download in player_downloads:
      if download["platform"] == "Windows":
        download["file"] = player_pkg_win
        download["version"] = version
        break
    
    # Update quest maker download.
    quest_maker_pkg_win = "https://gitlab.com/solarus-games/solarus-quest-editor/-/jobs/artifacts/v%(version)s/raw/solarus-x64-v%(version)s.zip?job=mingw-x64-package" % {
      "version": version,
    }
    quest_maker_downloads = data["downloads"]["quest-maker"]
    for download in quest_maker_downloads:
      if download["platform"] == "Windows":
        download["file"] = quest_maker_pkg_win
        download["version"] = version
        break
    
    # Write file.
    jsonutils.write_json(downloads_path, data)

    if verbose:
      print("Updated:", downloads_path)

# Add a new article that contains the changelogs for all synchronized projects.
def add_new_article(author, version, release_date, release_type, verbose = False):
  # Ensure locale is set to English for dates.
  locale.setlocale(locale.LC_ALL, "en_GB.utf8")

  repositories = {
    "solarus": {
      "changelog_url": r"https://gitlab.com/solarus-games/solarus/-/raw/dev/changelog.md",
      "changelog_content": "",
      "issue_url": r"https://gitlab.com/solarus-games/solarus/-/issues/",
      "display_name": "Solarus",
    },
    "solarus-quest-editor": {
      "changelog_url": r"https://gitlab.com/solarus-games/solarus-quest-editor/-/raw/dev/changelog.md",
      "changelog_content": "",
      "issue_url": r"https://gitlab.com/solarus-games/solarus-quest-editor/-/issues/",
      "display_name": "Solarus Quest Editor",
    }
  }

  translations = {
    "en": {
      "title": "### Changes for %(display_name)s %(version)s",
    },
    "fr": {
      "title": "### Changements dans %(display_name)s %(version)s",
    }
  }

  # Get changelog for each repository.
  for repository in repositories:
    full_changelog = download_changelog(repositories[repository]["changelog_url"], verbose)
    version_changelog = get_version_changelog(version, full_changelog, verbose)
    
    # Titles needs one more level.
    version_changelog = version_changelog.replace("###", "####")
    
    # Add links to GitLab issues.
    version_changelog = re.sub(r"\(#(\d+)\)", "([#\\1](%s/\\1))" % repositories[repository]["issue_url"], version_changelog)
    
    # Save data to dictionary.
    repositories[repository]["changelog_content"] = version_changelog
    
    if verbose:
      if version_changelog:
        print(repository, version, "changelog found")
      else:
        print(repository, version, "change NOT found")

  # Create files
  for lang in ["en", "fr"]:
    article_id = r"%(isodate)s-solarus-%(version)s-%(release_type)s-release" % {
      "version": version.replace(".", "-"),
      "isodate": release_date.isoformat(),
      "release_type": release_type,
    }
    article_path = r"./%(lang)s/entities/article/%(year)s/%(month)s/" % {
      "lang": lang,
      "year": release_date.year,
      "month": release_date.strftime("%m"),
    }

    md_file_content = ""
    for repository in repositories:
      # Section title
      section_title = translations[lang]["title"] % {
        "display_name": repositories[repository]["display_name"],
        "version": version
      }

      # Section content
      section_content = repositories[repository]["changelog_content"]

      # Append this text to the markdown file
      md_file_content += section_title + "\n\n" + section_content + "\n\n"

    # Article files
    article_folder = article_path + article_id
    if not os.path.exists(os.path.abspath(article_folder)):
      os.makedirs(os.path.abspath(article_folder))

    create_image_file(article_folder, version, verbose)

    json_file_template_path = os.path.abspath(r"./scripts/templates/article-release.template.%s.json" % lang)
    create_json_file(article_folder, json_file_template_path, article_id, version, release_date, author, verbose)

    md_file_template_path = os.path.abspath(r"./scripts/templates/article-release.template.%s.md" % lang)
    create_md_file(article_folder, md_file_template_path, article_id, version, md_file_content.strip(), verbose)

# Main
if __name__ == '__main__':
  # Create parameter parser
  parser = argparse.ArgumentParser(description="Script to update Solarus version on the website.")
  parser.add_argument("-version", type=str, help="New version number (x.y.z)")
  parser.add_argument("-author", type=str, help="Article author")
  parser.add_argument("-d", "--date", help="New version release date (yyyy-mm-dd)")
  parser.add_argument("-t", "--type", help="Type of release: major, minor, bugfix [default]")
  parser.add_argument("-V", "--verbose", action="store_true")

  # Parse parameters.
  args = parser.parse_args()
  
  # Check arguments.
  if args.version is None:
    print("Error: Version number (x.y.z) is required.")
    sys.exit()
  if args.author is None:
    print("Error: Author's full name (i.e. John Doe) is required.")
    sys.exit()

  # Choose today as the article date if no date, or parse the date.
  if args.date is None:
    release_date = date.today()
  else:
    release_date = date.fromisoformat(args.date)

  # Type of release.
  if args.type is None:
    release_type = "bugfix"
  elif args.type != "major" and args.type != "minor" and args.type != "bugfix":
    print("Error: A valid type of is required (major, minor, bugfix).")
    sys.exit()
  else:
    release_type = args.type

  # Verbose output.
  if args.verbose:
    print("New Version number:", args.version, r"(%s)" % release_type)
    print("Release date:", release_date.isoformat())
    
  # Let's go.
  update_homepage(args.version, release_date, args.verbose)
  add_new_article(args.author, args.version, release_date, release_type, args.verbose)
  update_downloads(args.version, args.verbose)
