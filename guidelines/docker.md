# Using Docker

The [Solarus Kokori CMS](https://gitlab.com/solarus-games/kokori) is also available as a
[Docker image](https://gitlab.com/solarus-games/kokori/container_registry).

The image comes pre-configured with Apache, PHP and Kokori, and is ready to use.

## Using the Image

A Docker image for Kokori is provided in the following GitLab container registry: `registry.gitlab.com/solarus-games/kokori`. Contributors can use this image without needing to build it themselves.

The image is very simple and can run the Solarus website pages directly:

```bash
git clone https://gitlab.com/solarus-games/solarus-website-pages.git
docker run --rm -p 8080:80 -v $PWD/solarus-website-pages:/var/www/html/public/data:ro registry.gitlab.com/solarus-games/kokori
```

Afterwards, navigate to [`http://localhost:8080`](http://localhost:8080/) to see the website.

A `--detach` argument can be also added to the above command to let it run in the background. Then,
`docker ps`, `docker logs` and `docker stop` can be used to manage the running container.

Restarting the container is NOT required if the files in `solarus-website-pages` change.

Running [Docker Desktop](https://www.docker.com/products/docker-desktop) with the
[Windows Subsystem for Linux version 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
is the best way to use Docker in Windows 10.

For more information about the Kokori Docker image, visit the [Kokori repository](https://gitlab.com/solarus-games/kokori).
