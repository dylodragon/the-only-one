# Translation

Since Kokori is file-based, the translation need files to be duplicated for each language. All the files related to a specific language must be grouped the same root folder together. For instance, all the French translation files are in the `fr` folder, and mirror all the equivalent files in the `en` folder.

At the moment, since the project is still young, everything is not optimized yet. It means every file, including images, must be duplicated.

Check that a translation into your language is not already being written.

Please fork the project, clone it, and create a new branch.

If not created yet, copy-paste the English folder and rename the copy with your language code. For instance, `es` if you plan to translate everything to Spanish.

## What to NOT translate

Beside shortcodes that obviously won't be translated, here is what we don't translate:

- **Images:** logos, 

- **Files and folders names:** we intend to keep the same folder hierarchy for every language.

- **Slugs:** so we can keep the same page address for every language. For instance, the resource pack library page is `www.solarus-games.org/en/development/resource-packs` for English, and is `www.solarus-games.org/fr/development/resource-packs` (the only difference is the `fr`). Pages keep the same page id for every language, so that we can also refer to parents easily.

- **JSON structure and properties keys:** we want to keep the same JSON structure for every language. Only properties values are translated.

- **Old news folders:** Don't translate the `old` articles folders found in the English and French translations. These folders come from the old English and French websites, and contain old news that we have migrated.

## What to translate

|File|To be translated|
|----|----------------|
|`config/menus.json`                |Only `label` values|
|`downloads/*.md`                   |All content|
|`entities/*/structure.json`        |Only `label` values|
|`entities/*/*.md`                  |All content|
|`entities/article/*.json`          |All but slugs|
|`entities/game/*/*.json`           |Values for `authors`, `age`, `excerpt`, `languages`, `meta_description`, `types`|
|`entities/game/*/screen_*.png`     |Remaking screenshots in your language is optional.|
|`entities/page/*.json`             |Values for `meta_description`, `meta_title`, `title`|
|`entities/resource-pack/*/*.json`  |Values for `authors`, `excerpt`, `meta_description`|
|`errors/*.md`                      |All content|
|`models/*.md`                      |Row header labels, button labels, etc. |
|`widgets/*.md`                     |All content|
