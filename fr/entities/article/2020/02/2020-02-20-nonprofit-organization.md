Jusqu'à aujourd'hui, Solarus n'était pas soutenu par une entité légale. Nous sommes heureux de vous annoncer que c'est désormais le cas !

![Logo Solarus Labs](2020-02-20-nonprofit-organization/solarus-labs-logo.png)

Nous avons créé l'association **Solarus Labs**, un organisme à but non-lucratif basé en France (car Christopho le créateur de Solarus est français). Vous trouverez davantage d'informations sur [la page de l'association](/fr/about/nonprofit-organization).

Cela ne change rien d'un point de vue non-légal. Le projet continuera d'être libre et open-source, et ce pour toujours, et ceux qui ne sont pas citoyens Français pourront toujours y contribuer.

La seule différence est que maintenant le projet dispose d'une entité juridique pour recevoir des dons. Évidemment, et comme avant, tous les dons seront réinvestis dans le projet.
