<p>Je viens de me rendre compte que deux des cinq screenshots du donjon 6 étaient les mêmes dans la news d'hier ! Voici le screenshot manquant :<p>

<center><a href="/images/solarus-ecran48.png" target="_blank"><img src="/images/solarus-ecran48.png" border=0 width=160></a>

<p>Voilà ! Vous pouvez retrouver toutes les images du jeu dans la <a href="http://www.zelda-solarus.com/galerie.php3">galerie</a>.</p>