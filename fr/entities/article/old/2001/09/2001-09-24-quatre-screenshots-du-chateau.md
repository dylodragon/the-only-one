<p>Salut à tous !</p>

<p>Les dérangements causés par la fermeture de Babeloueb n'ont pas empêcher Zelda : Mystery of Solarus de se construire un peu plus. Le jeu a même beaucoup avancé depuis le week-end dernier. Le quatrième donjon est totalement terminé depuis pas mal de temps. Tout se qui se passe entre ce donjon et le cinquième est même quasiment prêt ! Et c'est pourtant loin d'être court...</p>

<p>Une surprise se produit à la fin du donjon 4, et c'est en rapport avec le scénario (souvenez-vous : quatre enfants ont été enlevés). Il s'agit en quelque sorte... d'un rebondissement dans le scénario. Cette surprise est d'ailleurs de taille ! Elle ne sera pas révélée avant la sortie de la version finale du jeu, donc ne comptez pas sur moi pour vous en parler ! Ben oui, il faut bien garder un peu de surprises pour le jeu final.</p>

<p>Voici maintenant quatre nouveaux screenshots de mes récentes avancées :</p>

<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><a href="/images/solarus-ecran40.png" target="_blank"><img src="/images/solarus-ecran40.png" width="150" border="0"></a></td>
<td><a href="/images/solarus-ecran41.png" target="_blank"><img src="/images/solarus-ecran41.png" width="150" border="0"></a></td>
</tr>
<tr>
<td><a href="/images/solarus-ecran42.png" target="_blank"><img src="/images/solarus-ecran42.png" width="150" border="0"></a></td>
<td><a href="/images/solarus-ecran43.png" target="_blank"><img src="/images/solarus-ecran43.png" width="150" border="0"></a></td>
</tr>
</table>

<p>Les connaisseurs auront sans difficulté reconnu le Château d'Hyrule !<p>