Trois nouveaux épisodes de la solution vidéo sont disponibles ! Et ce sont les trois derniers car nous sommes arrivés à la fin du jeu. 

La solution vous guidera donc dans les derniers donjons, les dernières énigmes et le combat final. Merci à Metallizer qui a commenté ces épisodes avec moi, et surtout qui a réalisé les deux derniers donjons du jeu. :)

[list]
[li][url=http://www.youtube.com/watch?v=4HEzzgsQepM]Épisode 17 : Donjon des Pics Rocheux[/url][/li]
[li][url=http://www.youtube.com/watch?v=Z7CwjZpo2b8]Épisode 18 : Du niveau 8 au niveau 9[/url][/li]
[li][url=http://www.youtube.com/watch?v=qhiBvDKNSSI]Épisode 19 : Temple des Souvenirs[/url][/li]
[/list]

Un grand merci à toutes les personnes qui m'ont accompagné aux commentaires de cette solution en vidéo : Binbin, Mymy, Thyb, Morwenn, Elenya, Neovyse, BenObiWan, Sam101 et Metallizer.

Un ou deux épisodes bonus pourraient être encore ajoutés un peu plus tard... Tenez-vous au courant !