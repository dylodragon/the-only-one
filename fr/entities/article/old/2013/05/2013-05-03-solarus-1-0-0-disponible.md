<a href="../data/fr/entities/article/old/2013/05/images/solarus-logo-black-on-transparent.png"><img class="aligncenter size-medium wp-image-36532" alt="Logo du moteur Solarus" src="/images/solarus-logo-black-on-transparent-300x90.png" width="300" height="90" /></a>

La toute nouvelle génération de notre moteur de jeu Solarus vient de naître !

Merci à Neovyse pour le logo :)

Comme évoqué dans des articles précédents, je travaillais depuis la sortie de <a href="http://www.zelda-solarus.com/zs/jeu/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a> à approfondir le moteur, et à le revoir en profondeur de façon à pouvoir développer <a href="http://www.zelda-solarus.com/zs/jeu/zelda-mercuris-chest/">Mercuris' Chest</a>, mais aussi de façon à ce qu'un jour, chacun puisse faire des jeux en le réutilisant.

Plusieurs personnes n'ont d'ailleurs pas attendu cette version 1.0.0 pour commencer à développer leurs projets de jeux. Et c'est tant mieux ! À partir d'aujourd'hui, on peut considérer que le moteur est prêt à cela.

L'éditeur de quêtes fête quant à lui sa tout première version officielle. Il ne fait pas tout : certains fichiers doivent encore être créés à la main, mais il fait de plus en plus de choses. Vous pouvez l'essayer ! (Mais pas encore sur Zelda Mystery of Solarus DX ou XD car ces derniers utilisent pour l'instant toujours un vieux format de quêtes. (En fait, si, vous pouvez, mais c'est un peu plus compliqué : il vous faut soit avec une vieille version de l'éditeur, soit une version de développement de ZSDX.))
<ul>
	<li><span style="line-height: 13px;">Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.0.0-win32.zip">Solarus 1.0.0 + l'éditeur de quêtes</a> pour Windows</span></li>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.0.0-src.tar.gz">code source</a></li>
</ul>
Si vous avez envie de faire un jeu avec le moteur Solarus, armez-vous de motivation, puis rendez-vous sur le blog de développement (en anglais) pour en savoir plus : <a href="http://www.solarus-games.org/">http://www.solarus-games.org</a>. Autant vous prévenir tout de suite : ce n'est qu'une version 1.0.0. Ça fonctionne, et tout est très bien <a href="http://www.solarus-games.org/doc/1.0.0/quest.html">documenté</a>, mais créer un jeu ne se fait pas en quelques clics ! Bien sûr, avec les futures versions viendront des améliorations de l'éditeur. Je compte aussi réaliser un jour des tutoriaux (et en français :P) pour vous montrer comment ça marche.

En tout cas, que vous soyez créateurs de jeux ou pas, cette nouvelle est une bonne nouvelle car les avancées du moteur du jeu et de l'éditeur qui va avec sont bénéfiques pour le développement de Mercuris' Chest :P