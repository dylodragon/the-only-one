Guten tag!

Comme promis, une nouvelle version de Zelda Mystery of Solarus DX (1.5.2) vient de sortir, de même qu'une nouvelle version de ZSXD (1.5.3), le tout avec une mise à jour du moteur pour l'occasion (Solarus 0.9.3) ! Voici donc les principales nouveautés.

D'abord, quelques améliorations et corrections de bugs dans le moteur Solarus :
<ul>
	<li><span style="line-height: 13px;">Changement du répertoire des sauvegardes sur Mac OS X pour un répertoire plus standard. Il vous faudra déplacer vos fichiers manuellement pour continuer à jouer avec vos sauvegardes existantes.</span></li>
	<li>Amélioration du support de Mac OS X, Android, Pandora, Caanoo et autres plates-formes.</li>
	<li>Les images en format autre que 8 bits peuvent désormais aussi être utilisées pour les collisions au pixel près.</li>
	<li>Correction du bug des blocs qui ne se déplaçaient parfois qu'à moitié.</li>
	<li>Amélioration de certains types de mouvements sur les machines les plus lentes.</li>
	<li>Correction d'un blocage lors de l'utilisation du grappin sur un chou-péteur.</li>
</ul>
Cette mise à jour du moteur (Solarus 0.9.3) devrait être la dernière de la branche 0.9, branche qui devient obsolète maintenant que Solarus 1.0.0 est sorti. Les corrections mentionnées ci-dessus sont d'ailleurs déjà présentes dans Solarus 1.0.0.

Du côté de <a title="Téléchargements" href="http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/">Zelda Mystery of Solarus DX</a>, voici les nouveautés de la version 1.5.2, en plus des améliorations apportées par Solarus 0.9.3 :
<ul>
	<li><span style="line-height: 13px;">Version allemande disponible (merci Elenya !).</span></li>
	<li>Correction d'un personnage qui pouvait se superposer avec le héros dans le château.</li>
	<li>Correction d'un coffre qui pouvait se superposer avec le héros dans une grotte des montagnes.</li>
	<li>Corrections diverses dans les dialogues français, anglais et espagnols.</li>
</ul>
Et enfin, concernant <a title="Téléchargements" href="http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/">Zelda Mystery of Solarus XD</a> n'est pas en reste avec une nouvelle version 1.5.3 :
<ul>
	<li>Donjon 2 : le fragment de cur pouvait être obtenu très facilement avec l'épée.</li>
	<li>Corrections diverses dans les dialogues français et anglais.</li>
	<li>Ajout de creepers dans la caverne du sac de bombes.</li>
</ul>
Les téléchargements de ces nouvelles versions sont pour le moment disponibles pour Windows ou sous forme de code source. Pour les autres systèmes, cela devrait arriver rapidement :)