Une mise à jour de Solarus, ZSDX et de ZSXD vient de sortir. Elle apporte quelques corrections, dont une principale : dans la caverne de Billy le Téméraire, vous pouviez vous faire téléporter par erreur dans un endroit du jeu que vous n'étiez pas encore supposé pouvoir visiter, et cela pouvait même bloquer votre partie si vous sauvegardiez ensuite. Si vous êtes victime de ce problème, cette nouvelle version réparera votre sauvegarde :)

D'autres bugs plus mineurs ont également été corrigés, dont l'afficheur de curs qui ne fonctionnait pas toujours correctement, et la mort des ennemis qui était mal détectée lorsqu'ils tombent dans les trous.
<ul>
	<li>Télécharger <a href="http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/">Zelda Mystery of Solarus DX 1.7.1</a></li>
	<li>Télécharger <a href="http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/">Zelda Mystery of Solarus XD 1.7.1</a></li>
</ul>
Enjoy!