<p>Le site officiel des Alex d'or (<a href="http://www.alexdor.fr.st" target="_blank">www.alexdor.fr.st</a>) vient de communiquer les résultats du concours.</p>

<p>Netgamer et moi sommes fiers de vous présenter cette récompense : il s'agit de l'award du donjon-making !</p>

<center><img src="/images/award-alexdor.jpg"></center>

<p>Ce résultat est à hauteur de nos espérances, sachant que ce n'était que la démo qui était jugée. Rendez-vous dans un an pour l'édition des Alex d'or 2002, où cette fois le jeu final sera nominé !</p>