<p>Alors que @PyroNet, Seb le Grand et Geomaster travaillent d'arrache-pied pour maintenir le site à jour, je travaille toujours de mon côté sur le projet Zelda M'C. Bien que je ne donne pas beaucoup de nouvelles, le développement a repris et le moteur de jeu a avancé depuis la <a href="http://www.zelda-solarus.com/index.php?id=365">dernière mise à jour</a>.</p>

<p>Quelques nouveaux éléments du moteur ont été programmés. Certains d'entre eux ont été repris de la démo et améliorés, d'autres sont nouveaux, comme par exemple certaines attaques inédites... Voici d'ailleurs une toute nouvelle image de la version de développement :</p>

<p align="center"><img src="/images/develop006.png" width="320" height="240"></p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=scr">Voir les autres images</a></p>

<p>Ce que l'on peut dire à l'heure actuelle, c'est que beaucoup de choses sont déjà faites, et qu'il en reste encore beaucoup à faire :-)</p>