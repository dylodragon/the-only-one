Vous ne rêvez pas !

Ce n'est même pas le premier avril, pourtant c'est le moment de vous donner quelques nouvelles du projet qu'on vous promet depuis si longtemps.

Au cours des derniers mois, vous n'êtes pas sans savoir que c'était plutôt sur le <a href="http://www.zelda-solarus.com/zs/2015/05/solarus-1-4-disponible-nouvel-editeur-de-quetes/">nouvel éditeur de quêtes</a> et sur les <a href="http://wiki.solarus-games.org/doku.php?id=fr:video_tutorial">tutoriels vidéo</a> que je travaillais. Ce qui intéresse beaucoup les nombreuses personnes souhaitant créer leur propre jeu avec Solarus, mais les joueurs que vous êtes n'aviez pas grand chose à vous mettre sous la dent ! Bref, pendant tout ce temps, Zelda Mercuris' Chest n'avançait pas.

Maintenant que le nouvel éditeur de quêtes est sorti, je passe beaucoup moins de temps sur son développement ou celui du moteur (je me contente essentiellement des corrections de bugs). Tout cela pour dire que le développement de Zelda Mercuris' Chest s'accélère ! Il y a beaucoup de travail car c'est un projet ambitieux, mais l'équipe est motivée. Je fais de temps en temps du live-coding, c'est-à-dire du développement et du mapping de Zelda Mercuris' Chest en direct vidéo. Si cela vous intéresse, visitez ma <a href="http://www.twitch.tv/christophozs">chaîne Twitch</a> (pour les directs) et ma <a href="https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufxKu9TItbmyvjlJ3eSdsW0a">chaîne YouTube</a> (pour les extraits et les rediffusions). Mais attention aux spoilers !

Pour vous montrer un petit aperçu de l'avancement, voici une capture d'écran du village Mojo.

<a href="../data/fr/entities/article/old/2015/06/images/deku_village.png"><img class="aligncenter wp-image-37912 size-medium" src="/images/deku_village-300x225.png" alt="Village Mojo de Zelda Mercuris' Chest" width="300" height="225" /></a>

À bientôt pour d'autres nouvelles, je compte bien vous en donner régulièrement à partir d'aujourd'hui.