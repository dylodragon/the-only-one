Cette semaine, il est temps de vous montrer le donjon 2 de notre remake de Link's Awakening : la grotte du génie. Je pense que vous n'aurez pas de mal à reconnaitre ces deux images si vous avez déjà joué au jeu original.

&nbsp;

<a href="../data/fr/entities/article/old/2018/04/images/1.png"><img class="alignnone size-medium wp-image-38325" src="/images/1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/04/images/2.png"><img class="alignnone size-medium wp-image-38326" src="/images/2-300x240.png" alt="" width="300" height="240" /></a>

N'hésitez pas à donner votre avis !