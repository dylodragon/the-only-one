Ces derniers temps, nous avons travaillé sur le mapping de la partie est de Cocolint. Je suis certain que vous n'aurez pas de mal à reconnaître le temple du nord et le temple du sud :)

<a href="../data/fr/entities/article/old/2018/06/images/1.png"><img class="alignnone size-medium wp-image-38331" src="/images/1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/06/images/2.png"><img class="alignnone size-medium wp-image-38332" src="/images/2-300x240.png" alt="" width="300" height="240" /></a>

Comme d'habitude, n'hésitez pas à donner votre avis sur notre travail. :)