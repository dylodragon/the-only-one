C'est en ce 31 octoble que j'ai le plaisir de vous souhaiter à tous et à toutes, au nom de l'équipe, un très joyeux Halloween sur Zelda-Solarus !

Comme vous n'avez certainement pas manqué de le constater, il règne une sorte d'obscurité sur les pages du site... Tout comme Metallizer l'avait fait en son temps, j'ai décidé de déguiser le site pour l'occasion.

Passez de bonnes vacances, et merci de votre fidélité à Zelda-Solarus !