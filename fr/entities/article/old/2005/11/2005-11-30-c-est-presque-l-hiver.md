Après Halloween, Zelda-Solarus se met au blanc pour l'hiver, avec un nouveau design enneigé... (S'il ne s'affiche pas complètement, utilisez la touche F5 pour actualiser les images.)

J'espère que vous apprécierez, et je vous souhaite de bonnes vacances en avance, et de bons examens pour ceux qui sont concernés.

