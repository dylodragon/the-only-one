Bonjour à tous,

Je crains que cette news soit à nouveau dédiée à un public de développeurs plutôt que de joueurs :P.

Le développement du projet Zelda : Mystery of Solarus DX suit son cours. De plus en plus d'éléments sont maintenant définis dans la quête elle-même (en tant que fichiers externes) au lieu d'être définis &quot;en dur&quot; dans le moteur. Une précédente mise à jour donnait d'ailleurs de la documentation technique sur ces [url=http://www.solarus-engine.org/2010/08/06/quest-files-specification]fichiers de données[/url].

L'une des conséquences de définir l'aventure de façon de plus en plus indépendantes du moteur de jeu est qu'il devient possible de créer d'autres quêtes que Zelda : Mystery of Solarus DX en réutilisant le même moteur, mais avec d'autres maps, d'autres décors, d'autres dialogues et même bientôt d'autres objets disponibles dans l'équipement du joueur et dans l'inventaire. Je travaille actuellement très activement à toutes ces modifications.

L'autre conséquence est qu'il y a de nombreux fichiers de données à écrire et à traiter. Plusieurs outils seraient bien pratiques pour traiter, générer et vérifier ces fichiers. Et c'est là que j'aurais besoin de l'aide d'un ou plusieurs programmeurs :)

L'éditeur de maps permet actuellement de générer graphiquement les maps, ce qui est le minimum étant donné qu'il est impensable d'écrire une map à la main avec un éditeur de texte. Tous les autres fichiers de données de la quête sont créés à la main : les sprites, la liste des objets de l'équipement, les scripts, les traductions, etc. Ce que j'aimerais avoir (et ce qu'aimerait avoir toute personne qui crée une quête avec le moteur), c'est un éditeur de quête à part entière. Au lieu d'éditer seulement les maps, il permettrait aussi de gérer les autres fichiers de données : éditer les sprites, les scripts, les traductions dans les différentes langues, et ainsi de suite. Mais dans un premier temps, il serait déjà très pratique d'avoir un outil pour vérifier que la syntaxe de tous ces fichiers est correcte, afin de se rendre compte des erreurs plus facilement.

Le profil recherché est une personne :
[list]
[li]qui maîtrise la programmation en Java (à moins que vous vouliez refaire un éditeur de zéro dans un autre langage? on peut en discuter !),[/li]
[li]qui sache lire et écrire en anglais (le code source, les commentaires et la documentation est entièrement anglophone),[/li]
[li]qui ait du temps à consacrer au projet, il y a beaucoup de travail à faire,[/li]
[li]qui soit sérieuse et motivée.[/li]
[/list]
Je rappelle qu'il s'agit d'un projet amateur, libre et à but non lucratif et que toute l'équipe est évidemment bénévole. Si vous êtes intéressé(e) pour contribuer, n'hésitez pas à [url=http://www.solarus-engine.org/contact]me contacter[/url] via le blog de développement, le site ou le forum.