Vous n'en pouvez plus de tourner en rond dans Zelda Mystery of Solarus XD ? Les Creepers vous font trop peur et vous empêchent d'avancer ? Vous n'arrivez pas à faire immatriculer une galère ?

Ne vous en faites pas : nous avons préparé cette soluce pour vous épauler dans votre périple. L'intégralité de l'aventure est réalisée à travers une grande vidéo en six parties, disponibles sur YouTube. Et notre guide détaillé vous explique comme vous en sortir dans le Bureau des Objets Trouvés.

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsxd-soluce]Soluce vidéo de Zelda Mystery of Solarus XD[/url][/li]
[li][url=http://www.youtube.com/watch?v=SNlWPqK8CSc]Voir sur YouTube[/url][/li]
[/list]

Mais attention, le jeu ne contient que deux donjons, ne gâchez pas le plaisir en découvrant toutes les réponses trop vite !