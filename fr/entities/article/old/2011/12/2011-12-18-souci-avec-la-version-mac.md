La version Mac OS X de Zelda Mystery of Solarus DX est disponible depuis hier (merci à vlag67 ^^). Plusieurs personnes nous ont cependant signalé que le mode plein écran posait des problèmes, pouvant même faire planter le jeu.

Si cela vous arrive, le souci est que le mode vidéo est sauvegardé donc le jeu plante à nouveau dès que vous le relancez.
La solution pour réinitialiser le mode vidéo est de supprimer le fichier ~/.solarus/zsdx/config.ini. Ensuite, vous pouvez jouer, mais évitez de passer en plein écran. Nous travaillons à résoudre le souci, en attendant, désolé du désagrément et merci de nous l'avoir signalé ;)

EDIT: en attendant de trouver une meilleure solution, le plein écran a été désactivé dans la version Mac.