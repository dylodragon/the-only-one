Nous avons profité de l'Assemblée Générale annuelle de Solarus Labs pour faire un retour sur ce qui s'est passé en 2020, et quels sont les projets à venir pour Solarus en 2021.

## Solarus en 2020

### Le positif

Il y a exactement un an, nous avons créé [Solarus Labs](/fr/about/nonprofit-organization), une association Loi 1901 pour supporter Solarus de façon légale. Jusqu'à maintenant, elle a seulement servi à recevoir les dons. Nous espérons avoir un jour la possibilité d'organiser des évènements, mais la pandémie a reporté cela.

![Solarus Labs logo](images/solarus-labs-logo.png)

Solarus a connu deux mises à jour : [1.6.3](https://gitlab.com/solarus-games/solarus/-/tags/v1.6.3) et [1.6.4](https://gitlab.com/solarus-games/solarus/-/tags/v1.6.4). Elles ont apportés un meilleur support de macOS, davantage de stabilité, et surtout : un tilset libre par Max Mraz, appelé *Ocean Set*.

![Ocean Set screenshot](images/ocean-set-screenshot.png)

[Les produits dérivés Solarus](https://shop.spreadshirt.fr/solarus-labs) ont commencé comme une blague, mais finalement nous avons poussé le vice jusqu'à la rendre réelle. Finalement, nous en avons vendu quelques uns, ce qui a aidé à payer les frais des serveur et nom de domaine.

![Solarus tee-shirt](images/solarus-tee-shirt.png)

Christopho a fait une vidéo hommage à Bob Ross. Il parodie le célèbre peintre en créant une carte dans Solarus Quest Editor.

[youtube id="JisU7NR18Hw"]

La communauté est de plus en plus grande, et plus impliquée que jamais. Solarus a reçu plein d'amour de sa communauté :

- [Visual Novel System](/fr/development/resource-packs/visual-novel-system): une amélioration du système de dialogue pleine de fonctionnalités.
- De nouveaux [packs de ressources](/fr/development/resource-packs), de nouveaux scripts Lua.
- Avancement sur *[Children of Solarus](https://gitlab.com/solarus-games/children-of-solarus)*: la carte du monde est mappée !

![Children of Solarus world map](images/children-of-solarus-worldmap.png)

Solarus a été sous les projecteurs pour plusieurs raisons :

- Solarus a été ajouté à [Recalbox](https://www.recalbox.com/).
- [Liège Game Lab](https://www.liegegamelab.uliege.be/): une université utilise Solarus à des fins éducatives.

Quelques jeux Solarus ont beaucoup buzzé (tous deux par Max Mraz):

- *[Yarntown](https://maxatrillionator.itch.io/yarntown)*: un hommage 2D à *Bloodborne*.
- *[Ocean's Heart](https://store.steampowered.com/app/1393750/Oceans_Heart/)*: le tout premier jeu Solarus à être sorti sur Steam.

[youtube id="jELtHA_VBJ0"]

Pour toutes ces raisons, nous pouvons considérer que Solarus arrive à maturité.

### Le moins positif

Quelques projets n'ont pas progressé beaucoup :

- Les tutoriaux écrits et vidéos.
- La collaboration avec un institut français.
- L'application Andoird.
- Le nouveau lanceur/bibliothèque de quêtes.
- *The Legend of Zelda: Oni-Link Begins*.

## Les projets Solarus pour 2021 et plus

Nous avons toujours de nombreux projets pour Solarus. Voici une liste de ce que nous voudrions faire cette année, ou plus tard.

- Solarus 1.6.5 :
  - Correction du bug légendaire de la touche `R` lors du redimensionnement des tiles dans Quest Editor (corrigé !).
  - D'autres corrections de bugs.
  - Améliorations pour *Ocean's Heart*.
  - Projet top-secret... (dont vous aurez des nouvelles bientôt !)

- Solarus ~~1.7~~ **2.0** :
  - Fonctionnalités multi-joueurs (presque fini).
  - Nouveau Lanceur de Quêtes, avec davantages de fonctionnalités et une interface utilisateur plus travaillée.
  - Application Android (toujours en alpha).
  - Thème sombre pour l'éditeur de Quêtes sur Windows.
  - Nouveau logo.
  - Refraichissemtn du site web.

- Partage des connaissances :
  - Davantage de tutoriaux.
  - Nouveau site web pour la documentation.

- Développement de jeux:
  - *The Legend of Zelda: Oni-Link Begins*.
  - *Children of Solarus* (priorité numéro un).

- Produits dérivés:
  - Masque pour le Covid-19 ?
  - Nouveaux autocollants (pour accompagner le nouveau logo).

## Des nouvelles de Solarus Labs

Dans le but de vous offrir la transparence la plus totale, nous avons décidé d'ouvrir au public le [dépôt Git de Solarus Labs](https://gitlab.com/solarus-games/solarus-labs). C'est là que nous mettons les sources des documents de l'association : status légaux, compte-rendus des assemblées, etc. Cependant, c'est en lecteur seule : les forks et les merge requests sont bloquées. Seulement les membres du Bureau sont autorisés à modifier les fichiers.

## Conclusion

En dépit des circonstances peu réjouissantes, 2020 a été une grande année pour Solarus ! Merci à tous les membres de la communauté. L'année 2021 laisse présager de grandes choses également : la sortie d'*Ocean's Heart* peut potentiellement faire venir de nouvelles personnes dans la communauté.
