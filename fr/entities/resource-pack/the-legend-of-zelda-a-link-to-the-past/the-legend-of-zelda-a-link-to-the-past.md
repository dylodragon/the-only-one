### Présentation

*The Legend of Zelda: A Link to the Past*, publié en 1991 sur Super Nintendo, est la raison originelle de l'existence de Solarus. Avant d'être un moteur de jeu Action-RPG généraliste, Solarus fut d'abord un moteur de jeu basé sur ALTTP.

Les ressources de ce pack ont été rassemblées progressivement, en créant des jeux amateurs inspirés par ce jeu légendaire, et aujourd'hui ce pack contient tous les éléments nécessaires pour refaire *A Link to the Past* avec Solarus, des cartes aux PNJ, en passant par les menus et les objets.

![A Link to the Past box](images/alttp_box.png)
