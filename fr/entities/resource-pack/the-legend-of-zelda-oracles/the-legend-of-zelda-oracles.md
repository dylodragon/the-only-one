### Presentation

*The Legend of Zelda: Oracle of Ages* et *Oracle of Seasons* ont été publiés en 2001 sur Game Boy Color. Dans ces deux jeux complémentaires, le joueur peut respectivement voyager dans le temps et contrôler les saisons. La direction artistique est reprise de celle de *The Legend of Zelda: Link's Awakening DX*, publié plus tôt sur Game Boy Color également, bien que les sprites et les tilesets ont été étendus et améliorés.

Ce pack de ressources a été commencé par Katsu mais a actuellement besoin d'aide de la part d'artistes ou développeurs pour être terminé. Joignez-nous sur [le forum](http://forum.solarus-games.org/index.php/topic,1292.msg8091.html) pour aider ! 

![The Legend of Zelda: Oracle boxes](images/box_art.png)
