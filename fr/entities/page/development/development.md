[container]

# {title}

## Créer avec Solarus

Vous désirez créer un jeu avec Solarus ? Voici un peu d'aide pour vous aider à démarrer votre premier projet.

* Les **packs de ressources** sont des quêtes Solarus ne contenant que des scripts et des assets (tilesets, sprites, musiques, sons, etc.), vous permettant de démarrer avec des bases toutes faites.
* Les **tutoriels** sont un bon moyen pour débuter, vous expliquant étape par étape comment construire les éléments classiques : maps, menus, cinématiques, énigmes, donjons, ennemis, etc.
* La **documentation** de l'API Lua de Solarus est là où vous devez chercher dès que vous avez une question sur l'utilisation de cette API.

[space]

[row]
[column]

[button-highlight type="primary" icon="resource_pack" url="development/resource-packs" label="Packs de ressources" subtitle="Sprites, tilesets et scripts"]

[/column]
[column]

[button-highlight type="primary" icon="owl" url="development/tutorials" label="Tutoriels" subtitle="Apprenez à utiliser Solarus"]

[/column]
[column]

[button-highlight type="primary" icon="book" url="https://www.solarus-games.org/doc/latest" label="Documentation de l'API Lua" subtitle="Pour les créateurs de quête"]

[/column]
[/row]

[space thickness="50"]

## Contribuer à Solarus

Solarus est développé par des amateurs sur leur temps libre, donc l'équipe sera très heureuse si vous aidez d'une quelconque manière.

* Lisez **Comment contribuer** et venez parler avec nous sur Discord, que vous soyez un développeur C++/Lua, un créateur de quêtes Solarus, un artiste, un musicien, un généreux donateur ou n'importe qui d'autre.
* **Faire un don** au projet est possible, si vous avez l'âme généreuse. Nous vous serons très reconnaissants, et le don ira directement à l'association.
* Le **code source** est hebergé sur Gitlab. Jetez-y un oeil si vous voulez.
* La **documentation du code source** est là où se trouve la doc technique du moteur.

[space]

[row]
[column]

[button-highlight type="primary" icon="contribute" url="development/how-to-contribute" label="Comment contribuer" subtitle="Vous voulez nous aider ?"]

[/column]
[column]

[button-highlight type="primary" icon="gift" url="development/donation" label="Faire un don" subtitle="Aider financièrement le projet"]

[/column]
[column]

[button-highlight type="primary" icon="code" url="https://gitlab.com/solarus-games/" label="Code source" subtitle="Sur Gitlab"]

[/column]
[/row]

[/container]