[container]

[align type="center"]

# {title}

[hr width="5"]

[/align]

[row]
[column width="7"]

![Illustration](images/illustration.svg)

[/column]

[column width="1"]
[space orientation="vertical"]
[/column]

[column width="4"]

[space thickness="100"]

[button-highlight type="primary" icon="presentation" url="/fr/solarus/overview" label="Aperçu"]

[button-highlight type="primary" icon="download" url="/fr/solarus/download" label="Téléchargement"]

[button-highlight type="primary" icon="calendar" url="/fr/solarus/changelog" label="Journal des modifications"]

[/column]
[/row]

[/container]
