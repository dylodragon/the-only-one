## Description

**The Only One Project** est une petite aventure hardcore avec un pré-donjon, et un gros donjon avec pleins d'énigmes tordus, un mini-boss "rigolo", et un gros boss final original dans son pattern !

## Scénario

**Link** se réveille chez lui, dans un monde parallèle à A Link to the Past !
Il se retrouve sans arme, sans rien ! Sortie de chez lui, il part à la recherche d'informations, pour comprendre ce qu'il se passe !

Il va découvrir qu'une épaisse brume sombre recouvre le monde, et qu'il se passe quelque chose d'étrange dans un château non loin.
Les gardes bloquant l'accès au château, **Link** devra trouver une autre issue pour pouvoir s'y infiltrer !

### Système de jeu


### Durée de vie

Pour les joueurs les moins à l'aise avec les Zelda 2D, les modes difficiles/harcore, et les puzzles tordus, cela pourrait vous prendre entre 1 à 2 heures.