### Présentation

Tout studio de jeux vidéo a son vaporware, et *The Legend of Zelda: Mercuris Chest* est le nôtre ! Le jeu est en développement depuis 2002. Une démo, faite avec The Games Factory puis Multimedia Fusion, a été publiée en 2003, contenant un donjon long et complexe. Cependant, le développement a été annulé en 2007… pour être finalement redémarré depuis zéro en 2013, avec Solarus cette fois !

C'est le jeu le plus ambitieux de la Solarus Team, et il est attendu comme le jeu Zelda amateur 2D de référence. La carte du monde est un immense monde ouvert, avec de nombreux paysages différents, beaucoup de PNJ et de quêtes secondaires. Les donjons devraient être pour certains aussi grands que celui de la démo, et mieux conçus que ceux de *Mystery of Solarus DX*. Des sprites (personnages, ennemis, boss), des tilesets et d'autres éléments ont été créé spécialement pour l'occasion afin de donner au jeu son charme propre.
De plus, l'histoire est originale et plus développée que celle de *Mystery of Solarus*.

Curieusement, beaucoup d'idées datant de 2003 ont été utilisées dans des *Zelda* officiels depuis. Cela renforce le fait que nous avons pensé à de nombreux concepts qui sont conformes à la série.

![Link](artworks/artwork_link.png "Link")

### Synopsis

Alors que Link faisait tranquillement la sieste dans sa maison, deux étrangers s'introduisirent, le kidnappèrent et l'emmenèrent dans un royaume inconnu. Quand il se réveilla finalement, un prêtre lui expliqua que son aide était nécessaire pour combattre un oiseau maléfique géant qui tente de s'emparer d'un coffre légendaire, contenant une arme dangereuse. Qu'il y a t-il dans ce mystérieux coffre ? Quelles sont les intentions de ce guerrier volant ? Et pourquoi ce royaume semble si familier mais pourtant si différent ? Vous découvrirez la réponse à ces questions en jouant à *Mercuris' Chest* !

![Mercuris](artworks/artwork_mercuris.png "Mercuris")
