### Scénario

Le jeu se déroule juste après les événements du premier chapitre, petit jeu d'une durée de 40 minutes ne comportant qu'un unique donjon, et qui date de 2017.

Après avoir touché le Trophée de la Victoire gagné à la fin du donjon du Chapitre 1, Link se retrouve téléporté en Haïroule, un monde parallèle à Hyrule où plusieurs univers de jeux vidéos différents sont mélangés.
Link doit se rendre à la Tour des Souvenirs où Zeldo l'attend pour se venger de sa victoire passée !

### Système de jeu

L'action du jeu se déroule dans le Comté de Takapa, à l'Ouest d'Haïroule. Link est donc libre soit de se rendre à la Tour pour faire le challenge de Zeldo, soit d'explorer ce nouveau monde qui l'entoure. Le jeu est assez similaire à ALTTP, mais dans un univers plus décalé qu'un *Legend of Zelda* officiel, auquel viennent se greffer de nombreux univers issus de jeux vidéos différents, et références de toutes sortes.

Pour réaliser le 100%, il faudra cependant faire plusieurs voyages de la Tour au monde extérieur car la Tour contient plusieurs objets sans lesquels Link ne pourra pas explorer entièrement le Comté de Takapa.

### Durée de vie

Le jeu est assez court. Il peut se finir en une heure si vous souhaitez foncer vers l'objectif principal directement, mais obtenir le 100% pourrait vous prendre de 3h à 6h de jeu, suivant votre niveau de jeu et d'exploration.

À la fin du jeu, un écran de statistiques vous montrera votre progression et calculera votre pourcentage, comme dans les jeux *Zelda* de Vincent Jouillat. Un système de succès, repris également des ces jeux, a aussi été mis en place, pour le plaisir des joueurs les plus chevronnés !

### Quêtes principale et annexes

Le jeu ne contient qu'un seul grand donjon : La Tour des Souvenirs, qui constitue la quête principale du jeu. Cependant, plusieurs quêtes et mini-donjons annexes sont à découvrir, reprenant des mécaniques familières aux fans de *Zelda* : quête d'échanges, quarts de cœur... Les plus complétionnistes pourront même jusqu'à se lancer à la recherche des Lunes de Puissance, une mystérieuse quête qui mettra à rude épreuve leurs compétences d'explorateurs.
