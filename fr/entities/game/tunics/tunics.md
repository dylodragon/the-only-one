### Présentation

*Tunics!* est un Rogue-like dans l'univers de *The Legend of Zelda*. Cela implique donc qu'il n'y a que des donjons, et que ceux-ci sont générés aléatoirement. De surcroît, lorsque vous mourrez, vous devez redémarrer depuis le début, et vous perdez toutes les améliorations acquises (armes, vies, etc). Ce jeu doit être considéré comme une preuve de concept très aboutie.
