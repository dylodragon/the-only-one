# {title}

[youtube id="D7mqAscMpK8"]

Les objets destructibles (buissons, vases, pierres, herbe…) peuvent être coupés ou portés puis lancés par le héros. Ils peuvent cacher un trésor.

## Sommaire

- Utiliser les entités objets destructibles
  - Buisson (peut être coupé et soulevé)
  - Rocher (peut être soulevé)
  - Vase
  - Bombes-fleurs (explosent et se regénèrent) *(mentionnées)*
  - Herbe (peut être coupée et traversée)
- Utiliser la capacité de soulèvement `lift` (et poids des objets destructibles)
  - Afficher un dialogue quand le joueur ne peut pas soulever un objet *(mentionné)*
- Définir la propriété des dégâts infligés par les objets soulevables

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
