# {title}

[youtube id="8g-JIVNb3g8"]

## Sommaire

- Utiliser `map:on_draw()` pour afficher une image à l'écran
- Créer une surface à partir d'une image
- Utiliser `sol.main:on_draw()` pour afficher une image à l'écran de façon globale
- Créer un menu d'écran-titre qui affiche une image
  - Quitter le menu lorsqu'une touche est appuyée

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
