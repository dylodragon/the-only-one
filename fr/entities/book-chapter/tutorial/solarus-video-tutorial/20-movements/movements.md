# {title}

[youtube id="PkGl6SG5JJc"]

## Sommaire

- Utiliser les mouvements
  - La fonction `sol.movement.create(movement_type)`
  - Donner un mouvement aléatoire (`random`) à un PNJ
  - Donner un mouvement ciblé (`target`) à un PNJ pour qu'il suive le héros
  - Donner un mouvement de recherche de chemin (`path_finding`) à un PNJ pour qu'il traque le héros
  - Donner un mouvement droit (`straight`) à une surface dans le menu de démarrage

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
