[container]
[row]
[column]

#### Informations légales

**© 2006-2020 Christopho, Solarus**</br>
Logiciels GPL. Ressources CC-BY-SA.

* [link title="Informations légales" label="Informations légales" url="/fr/about/legal" icon="gavel" target=""]

[align type="center"]
[![GPL v3 logo](images/gpl_v3_logo.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)
[space orientation="vertical" thickness="10"]
[![CC-BY-SA 4.0 logo](images/cc_logo.png)](https://creativecommons.org/licenses/by-sa/4.0)
[/align]

[/column]
[column]

#### À propos

Solarus est maintenu par une équipe de bénévoles sur leur temps libre.

* [link title="Foire Aux Questions" label="Questions fréquentes" url="/fr/about/faq" icon="question-circle" target=""]
* [link title="Contributeurs" label="Contributeurs" url="/fr/about/contributors" icon="users" target=""]
* [link title="Association Loi 1901" label="Association Loi 1901" url="/fr/about/nonprofit-organization" icon="balance-scale" target=""]
* [link title="Contact" label="Contact" url="/fr/about/contact" icon="envelope-open-text" target=""]

[/column]
[column]

#### Contribuer

Solarus est gratuit : votre aide ou support sera très apprécié.

* [link title="Comment contribuer" label="Comment contribuer" url="/fr/development/how-to-contribute" icon="hand-holding-heart" target=""]
* [link title="Faire un don" label="Faire un don" url="/fr/development/donation" icon="donate" target=""]
* [link title="Code source" label="Code source" url="https://gitlab.com/solarus-games" icon="code-branch" target=""]
* [link title="Goodies" label="Goodies" url="https://shop.spreadshirt.fr/solarus-labs" icon="shopping-bag" target=""]

[/column]
[column]

#### Liens

Liens utiles pour rester informé sur le projet.

[link title="Gitlab" url="https://gitlab.com/solarus-games" icon="gitlab" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Twitter" url="https://www.twitter.com/SolarusGames" icon="twitter" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Facebook" url="https://www.facebook.com/solarusgames" icon="facebook" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Mastodon" url="https://mastodon.gamedev.place/@solarus" icon="mastodon" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Discord" url="https://discord.gg/PtwrEgZ" icon="discord" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Reddit" url="https://www.reddit.com/r/solarus" icon="reddit" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="RSS" url="https://www.solarus-games.org/feed/fr/articles" icon="rss" icon-category="fa" icon-size="2x" target=""]

[/column]
[column]

#### Sponsors

Nous remercions gracieusement nos sponsors pour leur aide.

[![GPL v3 logo](images/macstadium_poweredby_logo.png)](https://www.macstadium.com/)
[/column]
[/row]

[row]
[column width="3"]
[space]
[/column]
[column width="6"]
[align type="center"]
[space orientation="vertical" thickness="10"]

Réalisé avec [Kokori](https://gitlab.com/solarus-games/kokori), notre propre moteur de site libre et open-source.

[/align]
[/column]
[column width="3"]
[space]
[/column]

[/row]

[/container]
